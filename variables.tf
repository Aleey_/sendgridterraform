variable "api_key" {
  type        = string
  description = "api key récupérer"
}

variable "app_domain" {
  type        = string
  description = "nom de domaine ajouté"
}

variable "resource_group_name" {
  type        = string
  description = "nom du groupe de ressource"

}

variable "resource_group_location" {
  type        = string
  description = "location du rg"

}


variable "dnsname1" {
  type        = string
  description = "nom dns zone 1"

}

variable "dnsname2" {
  type        = string
  description = "nom dns zone 2"

}

variable "dnsname3" {
  type        = string
  description = "nom dns zone 3"

}

variable "record1" {
  type        = string
  description = "recorder n.1"

}

variable "record2" {
  type        = string
  description = "recorder n.2"

}

variable "record3" {
  type        = string
  description = "recorder n.3"

}


